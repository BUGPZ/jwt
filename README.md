## 仓颉JWT实现
1. 引入依赖
 - 在cjpm.toml文件中添加如下依赖
 ```toml
 [dependencies]
    jwt = {git = "https://gitcode.com/BUGPZ/jwt.git", branch = "main", version = "1.0.0"}
 ```
2. 安装依赖
 ``` shell
    cjpm update
 ```
3. 使用示例
``` cangjie
package TestPackage

import jwt.utils.*
import encoding.json.*

main() {
    let secret:String = "MTIzNDU2"
    let phone = "13500000000"
    let password = "e10adc3949ba59abbe56e057f20f883e"
    let exp = 3600
    let payload = JsonObject()
    payload.put("phone",JsonString(phone))
    payload.put("password",JsonString(password))
    payload.put("exp",JsonInt(exp))

    let header = JsonObject()
    header.put("alg",JsonString("HS256"))
    header.put("typ",JsonString("JWT"))
    header.put("test", JsonString("test"))
    
    let token = generateJwt(secret,data:payload,header:header)  // 生成token 目前只支持HS512
    println(token)
    println(verifyToken(token, secret))  // 校验token
    println(getTokenInfo(token))  // 获取payload
}
```

4. 其他说明：
    - 默认使用HS512算法，目前HMAC只支持HS512
    - Windows需要自行安装OpenSSL3 Crypto库需要
